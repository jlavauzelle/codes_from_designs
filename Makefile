SAGE=sage
PYS=$(wildcard *.py)
PYCS=$(wildcard *.pyc)

all: test readme.md

test: $(PYS)
	@echo "Testing files"
	$(SAGE) -t $^

clean:
	-rm -rf $(PYCS) readme.html
