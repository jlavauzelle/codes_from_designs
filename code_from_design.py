# -*- Python -*-
# -*- coding: utf-8 -*-



from sage.coding.linear_code import AbstractLinearCode, \
    LinearCodeGeneratorMatrixEncoder
from sage.coding.grs import GeneralizedReedSolomonCode 
from sage.coding.encoder import Encoder
from sage.coding.decoder import Decoder
from sage.misc.cachefunc import cached_method
import copy, random
from sage.matrix.constructor import matrix
from sage.modules.free_module import VectorSpace
from sage.combinat.designs.block_design import BlockDesign
from sage.combinat.designs.bibd import balanced_incomplete_block_design, \
    BalancedIncompleteBlockDesign
from sage.rings.finite_rings.finite_field_constructor import FiniteField as GF
from sage.categories.sets_cat import EmptySetError


# --------------------------------------------------------------------------- #
class CodeFromDesign(AbstractLinearCode):
    """
    Class handling codes based on designs.

    A code based on a design is defined through its parity-check matrix, whose
    rows are the incidence vectors between points and blocks.

    TESTS::
        
        sage: from code_from_design import CodeFromDesign
        sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
        sage: C1 = CodeFromDesign(B)
        sage: C1.length() == 5 and C1.design() == B
        True

        sage: C2 = CodeFromDesign(B, GF(3))
        sage: C2.length() == 5 and C2.design() == B and C2 != C1
        True

        sage: A = designs.AffineGeometryDesign(2, 1, 4)
        sage: C3 = CodeFromDesign(A, GF(2))
        sage: C3.dimension()
        7
    """
    _registered_encoders = {}
    _registered_decoders = {}
    
    def __init__(self, design, field=GF(2)):
        r"""
        INPUT:

        - ``design`` -- the design which defines the code

        - ``field`` -- (default: ``GF(2)``) the base field of the code

        EXAMPLES::

            sage: from code_from_design import CodeFromDesign
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: CodeFromDesign(B, GF(3))
            [5, 2] design-based code over GF(3)

            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: CodeFromDesign(A)
            [16, 7] design-based code over GF(2)

        """
        if not isinstance(design, BlockDesign):
            raise ValueError("design must be a BlockDesign")
        length = design.num_points()
        super(CodeFromDesign, self).__init__(
            field, length, "GeneratorMatrix", "Syndrome")
        self._design = design

    def __eq__(self, other):
        r"""
        TESTS:: 
        
            sage: from code_from_design import CodeFromDesign
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: C1 = CodeFromDesign(A)
            sage: C2 = CodeFromDesign(A, GF(3))
            sage: C3 = CodeFromDesign(B)
            sage: C1 == C2 or C1 == C3 or C2 == C3
            False            

        """
        return (isinstance(other, CodeFromDesign)
                and self.design() == other.design()
                and self.base_field() == other.base_field())
        
    def _repr_(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: repr(CodeFromDesign(A))
            '[16, 7] design-based code over GF(2)'

        """
        return ("[%d, %d] design-based code over GF(%d)" %
                (self.length(), self.dimension(),
                 self.base_field().cardinality()))

    def _latex_(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: latex(CodeFromDesign(A))
            [16, 7] \textnormal{design-based code over } \Bold{F}_{2}

        """
        return ("[%d, %d] \\textnormal{design-based code over } %s" %
                (self.length(), self.dimension(), self.base_field()._latex_()))

    def design(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: CodeFromDesign(A).design() == A
            True

        """
        return self._design

    @cached_method
    def dimension(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: CodeFromDesign(A).dimension()
            7

        """
        return self.generator_matrix().nrows()

    @cached_method
    def generator_matrix(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: CodeFromDesign(B).generator_matrix()
            [1 1 0 1 1]
            [0 0 1 0 1]

        """
        H = matrix(self.base_field(),
                   self._design.incidence_matrix().transpose())
        return H.right_kernel_matrix()

    @cached_method
    def locality(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: CodeFromDesign(A).locality()
            4

        """        
        return max(self.design().block_sizes())
# --------------------------------------------------------------------------- #



# --------------------------------------------------------------------------- #
class DesignBasedLocalDecoder(Decoder):
    """
    Class managing the simple local decoder associated to a code defined over
    a design.

    TESTS::

        sage: from code_from_design import CodeFromDesign, DesignBasedLocalDecoder
        sage: A = designs.AffineGeometryDesign(2, 1, 4)
        sage: C = CodeFromDesign(A, GF(2))
        sage: Dec = DesignBasedLocalDecoder(C); Dec
        Local decoder for the [16, 7] design-based code over GF(2)

        sage: c = C.random_element()
        sage: all(Dec.locally_correct(c, i) == c[i] for i in range(C.length()))
        True

    """
    def __init__(self, code):
        r"""
        INPUT:

        - ``code`` -- the design-based code which defines the decoder

        EXAMPLES::

            sage: from code_from_design import CodeFromDesign, DesignBasedLocalDecoder
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: C = CodeFromDesign(B, GF(3))
            sage: DesignBasedLocalDecoder(C)
            Local decoder for the [5, 2] design-based code over GF(3)

        """
        if not(isinstance(code, CodeFromDesign)):
            raise ValueError("You must provide a code based on a design.")
        super(DesignBasedLocalDecoder, self).__init__(
            code, code.ambient_space(), "GeneratorMatrix")

    def _repr_(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign, DesignBasedLocalDecoder
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: C = CodeFromDesign(B, GF(3))
            sage: repr(DesignBasedLocalDecoder(C))
            'Local decoder for the [5, 2] design-based code over GF(3)'

        """
        return "Local decoder for the %s" % self.code()

    def _latex_(self):
        r"""
        TESTS::        
        
            sage: from code_from_design import CodeFromDesign, DesignBasedLocalDecoder
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: C = CodeFromDesign(B, GF(3))
            sage: latex(DesignBasedLocalDecoder(C))
            \textnormal{Local decoder for the } [5, 2] design-based code over GF(3)

        """
        return "\\textnormal{Local decoder for the } %s" % self.code()

    def __eq__(self, other):
        r"""
        TESTS:: 
        
            sage: from code_from_design import CodeFromDesign, DesignBasedLocalDecoder
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
            sage: DA = DesignBasedLocalDecoder(CodeFromDesign(A))
            sage: DB = DesignBasedLocalDecoder(CodeFromDesign(B))
            sage: DA == DB
            False            

        """
        return (isinstance(other, DesignBasedLocalDecoder)
                and self.code() == other.code())

    def decode_to_code(self, word):
        pass

    def decoding_radius(self):
        pass

    def decode(self, word):
        pass
    
    def locally_correct(self, word, i):
        r"""

        Locally correct a word on a coordinate i.

        TESTS::

            sage: from code_from_design import CodeFromDesign, DesignBasedLocalDecoder
            sage: A = designs.AffineGeometryDesign(2, 1, 4)
            sage: C = CodeFromDesign(A, GF(2))
            sage: Dec = DesignBasedLocalDecoder(C); Dec
            Local decoder for the [16, 7] design-based code over GF(2)

            sage: c = C.random_element()
            sage: all(Dec.locally_correct(c, i) == c[i] for i in range(C.length()))
            True
        
        """
        C = self.code()
        F = C.base_field()
        design = C.design().relabel(inplace=False)        
        b = random.choice(design.blocks())
        while not(i in b):
             b = random.choice(design.blocks())
        return - sum([ word[t] for t in b if t != i ])
# --------------------------------------------------------------------------- #


CodeFromDesign._registered_encoders["GeneratorMatrix"] = LinearCodeGeneratorMatrixEncoder
CodeFromDesign._registered_decoders["LocalDecoder"] = DesignBasedLocalDecoder
DesignBasedLocalDecoder._decoder_type = {"hard-decision", "local"}
