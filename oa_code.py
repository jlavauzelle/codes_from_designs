# -*- Python -*-
# -*- coding: utf-8 -*-

from sage.structure.sage_object import SageObject
from sage.combinat.designs.block_design import BlockDesign
from code_from_design import CodeFromDesign

# --------------------------------------------------------------------------- #
class OrthogonalArray(SageObject):
    """
    Class for orthogonal arrays.

    This class aims at giving a class structure to orthogonal arrays, which
    does not exist in the current version of Sagemath (7.6).

    TESTS::

        sage: from oa_code import OrthogonalArray
        sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
        sage: oa = OrthogonalArray(L); oa
        2-OA(3,2,1)

        sage: L = designs.orthogonal_arrays.build(3,2,2)
        sage: oa_bis = OrthogonalArray(L)
        sage: oa == oa_bis
        True

    """

    def __init__(self, L, strength=2, check=False):
        """
        Build the orthogonal array from the list of lists/tuples L.
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [(0, 0, 0, 0, 0), (1, 0, 0, 0, 1), (0, 1, 0, 0, 1),\
                       (1, 1, 0, 0, 0), (0, 0, 1, 0, 1), (1, 0, 1, 0, 0),\
                       (0, 1, 1, 0, 0), (1, 1, 1, 0, 1), (0, 0, 0, 1, 1),\
                       (1, 0, 0, 1, 0), (0, 1, 0, 1, 0), (1, 1, 0, 1, 1),\
                       (0, 0, 1, 1, 0), (1, 0, 1, 1, 1), (0, 1, 1, 1, 1),\
                       (1, 1, 1, 1, 0)]
            sage: OrthogonalArray(L, strength=3)
            3-OA(5,2,2)

            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1]]
            sage: OrthogonalArray(L, check=True)
            Traceback (most recent call last):
            ...
            ValueError: The provided list of lists does not correspond to an orthogonal array.

        """
        self._rows = set(tuple(l) for l in L)
        self._symbols_set = set(s for l in L for s in l)
        self._n = len(self._symbols_set)
        self._k = len(L[0])
        self._strength = strength
        self._index = len(L)/(self._n ** self._strength)

        if (check and strength != 2):
            raise NotImplementedError("Verification with t>2 "
                                      "not yet implemented")
        elif check:
            S = self._symbols_set
            is_an_oa = True
            if (is_an_oa):
                i = 0
                while (is_an_oa and i < self._k-1):
                    j = i+1
                    while (is_an_oa and j < self._k):
                        dico = { tuple([s, t]): 0 for s in S for t in S }
                        for l in L:
                            dico[tuple([l[i], l[j]])] += 1
                        is_an_oa = all(x == self._index for x in dico.values())
                        j += 1
                    i += 1
            if not(is_an_oa):
                raise ValueError("The provided list of lists does not"
                                 " correspond to an orthogonal array.")


    def __eq__(self, other):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L1 = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: L2 = [[0, 1, 1], [1, 0, 1], [1, 1, 0], [0, 0, 0]]
            sage: OrthogonalArray(L1) == OrthogonalArray(L2)
            True
        """
        return (isinstance(other, OrthogonalArray)
                and self.rows() == other.rows())

            
    def n(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: OrthogonalArray(L).n()
            2
        """
        return self._n

    def k(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: OrthogonalArray(L).k()
            3
        """
        return self._k

    def strength(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: OrthogonalArray(L).strength()
            2
        """
        return self._strength
    
    def index(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: OrthogonalArray(L).index()
            1
        """
        return self._index
    
    def symbols_set(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: OrthogonalArray(L).symbols_set()
            {0, 1}
        """
        return self._symbols_set

    def rows(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: OrthogonalArray(L).rows()
            {(0, 0, 0), (0, 1, 1), (1, 0, 1), (1, 1, 0)}
        """
        return self._rows

    def __repr__(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: repr(OrthogonalArray(L))
            '2-OA(3,2,1)'
        """
        return ("%d-OA(%d,%d,%d)" %
                (self.strength(), self.k(), self.n(), self.index()))
# --------------------------------------------------------------------------- #
    


# --------------------------------------------------------------------------- #
class DesignFromOrthogonalArray(BlockDesign):
    """
    Class for handling designs based on orthogonal arrays.

    These designs are built through the generalization of the canonical
    transformation of an orthogonal array into a transversal design.
    """
    def __init__(self, oa):
        """
        TESTS::

            sage: from oa_code import OrthogonalArray, DesignFromOrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: oa = OrthogonalArray(L)
            sage: D = DesignFromOrthogonalArray(oa)
            sage: D
            Block design coming from 2-OA(3,2,1)
            sage: D.groups()
            [[(0, 0), (1, 0)], [(0, 1), (1, 1)], [(0, 2), (1, 2)]]
        """
        S = oa.symbols_set()
        k = oa.k()

        points = [ tuple([s, i]) for s in S for i in range(k) ]
        groups = [ [ tuple([s, i]) for s in S ] for i in range(k) ]
        blocks = [ [ tuple([r[i], i]) for i in range(k) ] for r in oa.rows() ]

        self._oa = oa
        self._groups = groups
        super(DesignFromOrthogonalArray, self).__init__(points, blocks)

    def groups(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray, DesignFromOrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: oa = OrthogonalArray(L)
            sage: DesignFromOrthogonalArray(oa).groups()
            [[(0, 0), (1, 0)], [(0, 1), (1, 1)], [(0, 2), (1, 2)]]
        """
        return self._groups

    def orthogonal_array(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray, DesignFromOrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: oa = OrthogonalArray(L)
            sage: DesignFromOrthogonalArray(oa).orthogonal_array()
            2-OA(3,2,1)
        """
        return self._oa

    def __repr__(self):
        r"""
        TESTS::

            sage: from oa_code import OrthogonalArray, DesignFromOrthogonalArray
            sage: L = [[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]]
            sage: oa = OrthogonalArray(L)
            sage: DesignFromOrthogonalArray(oa).__repr__()
            'Block design coming from 2-OA(3,2,1)'
        """
        return ("Block design coming from %s" % self.orthogonal_array())
# --------------------------------------------------------------------------- #


# --------------------------------------------------------------------------- #
class OrthogonalArrayFromLinearCode(OrthogonalArray):
    """
    Class for managing orthogonal arrays built from linear codes.
    """
    def __init__(self, C, dual_min_distance=3, check=False):
        """
        Note: `dual_min_distance` must be a lower bound on the minimum
        distance of the dual code of `C`.

        TESTS::

            sage: from oa_code import OrthogonalArrayFromLinearCode, \
                                      OrthogonalArray
            sage: C = codes.GeneralizedReedSolomonCode(GF(5).list(), 2)
            sage: d_dual = 3
            sage: oa = OrthogonalArrayFromLinearCode(C, dual_min_distance=d_dual)
            sage: oa
            2-OA(5,5,1)

            sage: oa2 = OrthogonalArray(designs.orthogonal_arrays.build(5,5), strength=2)
            sage: oa2 == oa
            True
        """
        L = C.list()
        super(OrthogonalArrayFromLinearCode, self).__init__(
            L, strength=dual_min_distance-1, check=check)
# --------------------------------------------------------------------------- #



# --------------------------------------------------------------------------- #
class CodedQueriesCode(CodeFromDesign):
    """
    Class building so-called coded-queries codes.

    Let `C_0` be a code. Call `D` the design built from the orthogonal array
    associated to `C_0`. Then, the `C_0`-coded-queries code over `GF(q)` is the 
    linear code over `GF(q)` based on `T`.
        
    """
    def __init__(self, C_0, field=None, dual_min_distance=3, check=True):
        """
        Note: `dual_min_distance` must be a lower bound on the minimum
        distance of the dual code of `C`.

        TESTS::

        """
        if field == None:
            field = C_0.base_field()
        A = OrthogonalArrayFromLinearCode(C_0, dual_min_distance, check)
        D = DesignFromOrthogonalArray(A)
        self._base_code = C_0
        self._underlying_oa = A
        super(CodedQueriesCode, self).__init__(D, field)

# --------------------------------------------------------------------------- #
